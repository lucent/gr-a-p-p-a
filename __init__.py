from st3m.application import Application, ApplicationContext
from st3m.ui.colours import PUSH_RED, GO_GREEN, BLACK, RED, GREEN, BLUE, WHITE, GREY
from st3m.goose import Dict, Any
from st3m.input import InputState
from ctx import Context
import leds

import json
import math

class RoseIslandApp(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        try:
            self._bundle_path = app_ctx.bundle_path
        except:
            self._bundle_path = f"/flash/sys/app/gr-a-p-p-a"
        
        self._font: int = 5
        self._scale = 1.0
        self._led = 0.0
        self._phase = 0.0
        self._counter = 0
        self._texts = ["Rose Island", "Gruppo Acaro", "Homemade Grappa", "Homemade Aged", "Spicy Grappa", "Superspicy Grappa", "& Coffee", ""]
        self._images = [f"rose-island.png", f"grappa.png", f"homemade.png", f"aged.png", f"hot.png", f"superhot.png", f"mokas.png", f"qr-loc.png"]
        self._colors = [GREEN, GREEN, GREEN, GREEN, GREEN, PUSH_RED, PUSH_RED, WHITE]
        self._textssize = [30, 30, 30, 30, 30, 30, 30, 30]
        self._cycle = 80

    def draw(self, ctx: Context) -> None:
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font = ctx.get_font_name(self._font)

        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        
        ctx.image(self._bundle_path + f"/images/" + self._images[math.trunc(self._counter / self._cycle)], -120, -120, 241, 241)
        ctx.rgb(*self._colors[math.trunc(self._counter / self._cycle)])

        ctx.move_to(0, 0)
        ctx.save()
        ctx.scale(self._scale, 1)
        
        ctx.font_size = self._textssize[math.trunc(self._counter / self._cycle)]
        ctx.text(self._texts[math.trunc(self._counter / self._cycle)])
        self._counter = self._counter + 1
        if self._counter > ((len(self._texts) - 1) * self._cycle) + (self._cycle - 1):
            self._counter = 0
            
        ctx.restore()

        leds.set_hsv(int(self._led), abs(self._scale) * 360, 1, 0.2)

        leds.update()
        # ctx.fill()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        self._phase += delta_ms / 1200
        self._scale = math.sin(self._phase)
        self._led += delta_ms / 45
        if self._led >= 40:
            self._led = 0
